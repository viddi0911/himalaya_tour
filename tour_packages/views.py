from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse 
from tour_packages.models import Package

# Create your views here.
def packages(request,package_category= None):
    if package_category:
        if package_category == 'camping':
            active = 'camping'
        packages_title = package_category.title() +'  ' + 'Packages'
        packages = Package.objects.filter(active=True,category=package_category).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    else:
        active = 'packages'
        packages_title = 'Our Packages'
        packages = Package.objects.filter(active=True).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    return render_to_response('packages.html', locals(), context_instance=RequestContext(request))

def hot_deals(request):
    active = 'hot_deals'
    packages_title = 'Our hot deals '
    packages = Package.objects.filter(active=True,is_offer=True).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration','offer_percentage','offer_price')
    return render_to_response('offer_packages.html', locals(), context_instance=RequestContext(request))

def thank_you(request):
    active = None#'packages'
    packages = Package.objects.filter(active=True).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    return render_to_response('thank-you.html', locals(), context_instance=RequestContext(request))

def package_detail(request,package_slug=None):
    active = 'packages'
    package = Package.objects.get(slug=package_slug)
    return render_to_response('package-detail.html', locals(), context_instance=RequestContext(request))

