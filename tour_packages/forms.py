from django import forms
from tour_packages.models import Enquiry
class EnquiryForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EnquiryForm, self).__init__(*args, **kwargs)
        for _field in self.fields:
            self.fields[_field].widget.attrs['class'] = 'form-control'
            self.fields[_field].widget.attrs['placeholder'] = self.fields[_field].label

    class Meta:
        model = Enquiry
        exclude = ['contacted','created_on','remarks']



