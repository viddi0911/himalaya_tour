from django.contrib import admin
from tour_packages.models import Package, Itinerary, OtherFacility, InclusionExclusion,Enquiry

# Register your models here.

from djangoseo.admin import register_seo_admin
from tour_packages.seo import MyMetadata



class ItineraryAdmin(admin.ModelAdmin):	
    list_display = ['level','description']

class PackageAdmin(admin.ModelAdmin):
    list_display = ['name','slug','short_description','price','priority','active','duration','is_slider','home_page_package','category']
    list_filter = ['duration','is_slider','home_page_package','is_slider']
    save_as = True

class EnquiryAdmin(admin.ModelAdmin):	
    list_display = ['name','contact_no','places_interested','created_on','contacted']
    list_filter = ['contacted','places_interested']

admin.site.register(Package, PackageAdmin)
admin.site.register(Itinerary,ItineraryAdmin)
admin.site.register(OtherFacility)
admin.site.register(Enquiry,EnquiryAdmin)
admin.site.register(InclusionExclusion)
register_seo_admin(admin.site, MyMetadata)
