from __future__ import unicode_literals
from autoslug import AutoSlugField
from django.db import models

# Create your models here.
import PIL
from PIL import Image

PACKAGE_CHOICES = (('honeymoon','honeymoon'),('family','family'),('adventure','adventure'),('group','group'),('kids','kids'),('camping','camping'))

def resize_image(path,img_width, img_height):
    img = Image.open(path)
    img = img.resize((img_width,img_height), PIL.Image.ANTIALIAS)
    img.save(path)

class InclusionExclusion(models.Model):
    description = models.CharField(max_length=150)

    def __unicode__(self):
        return self.description

class OtherFacility(models.Model):
    title = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title

class Itinerary(models.Model):
    priority = models.IntegerField()
    level = models.CharField(max_length=100)
    description = models.TextField()

    def __unicode__(self):
        return self.level

class Package(models.Model):
    name  = models.CharField(max_length=50)
    duration  = models.CharField(max_length=15,null=True,blank=True)
    slug = AutoSlugField(populate_from='name',unique=True)
    title = models.CharField(max_length=150)
    active = models.BooleanField(default=True,blank=False,null=False)
    category = models.CharField(max_length=10, choices=PACKAGE_CHOICES,default='adventure')
    is_slider = models.BooleanField(default=True,blank=False,null=False)
    home_page_package = models.BooleanField(default=True,blank=False,null=False)
    short_description = models.TextField()
    long_description = models.TextField()
    price = models.IntegerField()
    priority = models.IntegerField()
    itinerary= models.ManyToManyField(Itinerary,blank=True)
    inclusion = models.ManyToManyField(InclusionExclusion,related_name ='inclusion', blank=True)
    exclusion = models.ManyToManyField(InclusionExclusion,related_name='exclusion', blank=True)
    package_img_thumb = models.ImageField(upload_to='static/images/packages/small/', blank=True, null=True, help_text="Image of the package")
    package_img_banner = models.ImageField(upload_to='static/images/packages/large/', blank=True, null=True, help_text="Image of the package")
    other_facility = models.ManyToManyField(OtherFacility,blank=True)
    
    offer_percentage = models.IntegerField(blank=True, null=True)
    is_offer = models.BooleanField(default=False,blank=False,null=False)
    offer_price = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.name

    def save(self,*args,**kwargs):
        super(Package,self).save(*args, **kwargs)
        try:
            resize_image(self.package_img_thumb.path,270,200)
            resize_image(self.package_img_banner.path,1600,780)
        except:
            pass
        super(Package,self).save(*args, **kwargs)

class Enquiry(models.Model):
    name  = models.CharField(max_length=50)
    contact_no  = models.CharField(max_length=15)
    email = models.EmailField(max_length=150)
    places_interested = models.CharField(max_length=150)
    no_of_person = models.CharField(max_length=150)
    message = models.TextField()
    created_on= models.DateTimeField(auto_now_add=True)
    contacted = models.BooleanField(default=False)
    remarks = models.CharField(max_length=150,null=True,blank=True)

from djangoseo import seo

class MyMetadata(seo.Metadata):
    title       = seo.Tag(head=True, max_length=68)
    description = seo.MetaTag(max_length=155)
    keywords    = seo.KeywordTag()
    heading     = seo.Tag(name="h1")
