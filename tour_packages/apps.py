from __future__ import unicode_literals

from django.apps import AppConfig


class TourPackagesConfig(AppConfig):
    name = 'tour_packages'
