from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse,HttpResponseRedirect 
from tour_packages.models import Package
from tour_packages.forms import EnquiryForm

def home(request):
    active = 'home'
    packages = Package.objects.filter(active=True).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    slider_packages = Package.objects.filter(active=True,is_slider = True,is_offer=False).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    home_page_packages = Package.objects.filter(active=True,home_page_package=True, is_offer=False).order_by('priority').values('name','title','slug','short_description','package_img_thumb','price','package_img_banner','duration')
    partners = range(1,7)
    return render_to_response('index.html', locals(), context_instance=RequestContext(request))

def about_us(request):
    active = 'about_us'
    return render_to_response('about.html', locals(), context_instance=RequestContext(request))

def about_himalaya(request):
    active = 'about_himalaya'
    return render_to_response('himalaya.html', locals(), context_instance=RequestContext(request))

def enquiry(request):
    active = 'contact'
    if request.method == 'POST':
        form = EnquiryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/thank-you/')
    form = EnquiryForm()
    return render_to_response('contact.html', locals(), context_instance=RequestContext(request))

def contactus(request):
    active = 'contact-us'
    return render_to_response('contact_us.html', locals(), context_instance=RequestContext(request))
