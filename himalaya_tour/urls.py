"""himalaya_tour URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from himalaya_tour.views import home, about_us, enquiry, about_himalaya, contactus
from django.conf.urls.static import static
from tour_packages.views import packages, package_detail,thank_you, hot_deals

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$',home),
    url(r'^about-us/$',about_us),
    url(r'^about-himalaya/$',about_himalaya),
    url(r'^enquiry/$',enquiry),
    url(r'^contact-us/$',contactus),
    url(r'^packages/(?P<package_category>[\S]+)/$',packages),
    url(r'^packages/',packages),
    url(r'^thank-you/$',thank_you,name='thank-you'),
    url(r'^package-detail/(?P<package_slug>[\S]+)/$',package_detail),
    url(r'^hot-deals/',hot_deals),
   # url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
]

